﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softomotive.Server.Abstractions.Loggers;
using Softomotive.Server.Abstractions.Services;
using Softomotive.Server.Http.Controllers;
using Softomotive.Server.HttpModels;
using System.Collections.Generic;

namespace Softomotive.Server.UnitTests
{
    [TestClass]
    public class AdminControllerTest
    {
        [TestMethod]
        public void TestGetClientsConnectionIds()
        {
            var connectedClients = new List<string>() { "x", "y", "z" };

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientsConnectionIds())
                .Returns(connectedClients);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var response = controller.GetClientsConnectionIds();

            Assert.IsTrue(response == connectedClients);
        }

        [TestMethod]
        public void TestSelectWindow()
        {
            const string windowName = "Foo";
            const string clientConnectionId = "Bar";
            const string clientHttpServerUrl = "http://test";

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientHttpServerUrl(clientConnectionId))
                .Returns(clientHttpServerUrl);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var model = new SelectWindowRequestModel() { ClientConnectionId = clientConnectionId, WindowName = windowName };
            var response = controller.SelectWindow(model).Result;

            Assert.IsTrue(response.InvalidClient);
        }

        [TestMethod]
        public void TestMoveMouse()
        {
            const int X = 0;
            const int Y = 0;
            const string clientConnectionId = "Bar";
            const string clientHttpServerUrl = "http://test";

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientHttpServerUrl(clientConnectionId))
                .Returns(clientHttpServerUrl);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var model = new MoveMouseRequestModel() { ClientConnectionId = clientConnectionId, X = X, Y = Y };
            var response = controller.MoveMouse(model).Result;

            Assert.IsTrue(response.InvalidClient);
        }

        [TestMethod]
        public void TestMouseClick()
        {
            const string clientConnectionId = "Bar";
            const string clientHttpServerUrl = "http://test";

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientHttpServerUrl(clientConnectionId))
                .Returns(clientHttpServerUrl);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var model = new MouseClickRequestModel() { ClientConnectionId = clientConnectionId };
            var response = controller.MouseClick(model).Result;

            Assert.IsTrue(response.InvalidClient);
        }

        [TestMethod]
        public void TestSendText()
        {
            const string clientConnectionId = "Bar";
            const string clientHttpServerUrl = "http://test";
            const string text = "Foo";

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientHttpServerUrl(clientConnectionId))
                .Returns(clientHttpServerUrl);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var model = new SendTextRequestModel() { ClientConnectionId = clientConnectionId, Text = text };
            var response = controller.SendText(model).Result;

            Assert.IsTrue(response.InvalidClient);
        }

        [TestMethod]
        public void TestExecuteActionScript()
        {
            const string clientConnectionId = "Bar";
            const string clientHttpServerUrl = "http://test";
            const string actionScript = "Foo";

            var dataServiceMock = new Mock<IDataService>();
            var hubServiceMock = new Mock<IHubService>();
            var loggerMock = new Mock<ILogger>();

            dataServiceMock
                .Setup(x => x.GetClientHttpServerUrl(clientConnectionId))
                .Returns(clientHttpServerUrl);

            var dataService = dataServiceMock.Object;
            var hubService = hubServiceMock.Object;
            var logger = loggerMock.Object;

            var controller = new AdminController(dataService, hubService, logger);
            var model = new ExecuteActionScriptRequestModel() { ClientConnectionId = clientConnectionId, ActionScript = actionScript };
            var response = controller.ExecuteActionScript(model).Result;

            Assert.IsTrue(response.InvalidClient);
        }
    }
}
