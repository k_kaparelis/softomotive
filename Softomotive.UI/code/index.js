﻿var isConnected = false;
var serverUrl = "http://localhost:8080";
var hubName = "MyHub";
var connectionId;
var $clientConnectionIds = $("#clientConnectionIds");
var $actionScriptTxt = $("#actionScriptTxt");

function connectToServer() {
    if (isConnected)
        return;

    var connection = $.hubConnection(serverUrl);
    var hubProxy = connection.createHubProxy(hubName);

    hubProxy.on('clientAdded', function (clientConnectionId) {
        $clientConnectionIds.append(new Option(clientConnectionId, clientConnectionId));
    });

    hubProxy.on('clientRemoved', function (clientConnectionId) {
        $clientConnectionIds.find("option").each(function () {
            var value = $(this).val();

            if (value === clientConnectionId)
                $(this).remove();
        });
    });

    connection.start().done(function () {
        isConnected = true;
        connectionId = connection.id;

        $.post(serverUrl + "/api/admin/register", { connectionId: connectionId })
            .done(function (data) {
                alert("Connected")
            });

        $.get(serverUrl + "/api/admin/getClientsConnectionIds")
            .done(function (data) {
                if (data.length === 0)
                    return;

                data.forEach(function (clientsConnectionId) {
                    $clientConnectionIds.append(new Option(clientsConnectionId, clientsConnectionId));
                });
            });
    });

}

function getSelectedConnectionId() {
    return $clientConnectionIds.find("option:selected").val();
}

function selectWindow() {
    var windowName = prompt("Window name:");

    var model = { clientConnectionId: getSelectedConnectionId(), windowName: windowName };

    $.post(serverUrl + "/api/admin/selectWindow", model)
        .done(function (data) {
            if (data.InvalidClient)
                alert("Could not find client.");
            else if (data.InvalidWindow !== null)
                alert("Could not find window: " + data.InvalidWindow);
            else
                alert("Completed");
        })
        .fail(function () {
            alert("error");
        });
}

function mouseMove() {
    var xPosition = prompt("X:");
    var yPosition = prompt("Y:");

    var model = { clientConnectionId: getSelectedConnectionId(), x: xPosition, y: yPosition };

    $.post(serverUrl + "/api/admin/moveMouse", model)
        .done(function (data) {
            if (data.InvalidClient)
                alert("Could not find client.");
            else if (data.NoSelectedWindow)
                alert("You have to select window first.");
            else
                alert("Completed");
        })
        .fail(function () {
            alert("error");
        });
}

function mouseClick() {
    var model = { clientConnectionId: getSelectedConnectionId() };

    $.post(serverUrl + "/api/admin/mouseClick", model)
        .done(function (data) {
            if (data.InvalidClient)
                alert("Could not find client.");
            else
                alert("Completed");
        })
        .fail(function () {
            alert("error");
        });
}

function sendText() {
    var text = prompt("Text:");

    var model = { clientConnectionId: getSelectedConnectionId(), text: text };

    $.post(serverUrl + "/api/admin/sendText", model)
        .done(function (data) {
            if (data.InvalidClient)
                alert("Could not find client.");
            else
                alert("Completed");
        })
        .fail(function () {
            alert("error");
        });
}

function sendActionScript() {
    var actionScript = $actionScriptTxt.val();

    var model = { clientConnectionId: getSelectedConnectionId(), actionScript: actionScript };

    $.post(serverUrl + "/api/admin/executeActionScript", model)
        .done(function (data) {
            if (data.InvalidClient)
                alert("Could not find client.");
            else if (data.InvalidWindow !== null)
                alert("Could not find window: " + data.InvalidWindow);
            else if (data.NoSelectedWindow)
                alert("You have to select window first.");
            else if (data.InvalidActionScript)
                alert("Could not execute action script.");
            else if (data.InvalidActionScriptAction !== null)
                alert("Could not execute action: " + data.InvalidActionScriptAction);
            else
                alert("Completed");
        })
        .fail(function () {
            alert("error");
        });
}