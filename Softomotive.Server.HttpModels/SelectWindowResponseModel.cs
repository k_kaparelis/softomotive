﻿namespace Softomotive.Server.HttpModels
{
    public class SelectWindowResponseModel
    {
        public bool InvalidClient { get; set; }
        public string InvalidWindow { get; set; }
    }
}
