﻿namespace Softomotive.Server.HttpModels
{
    public class ClientConnectionRequestModel
    {
        public string ConnectionId { get; set; }
        public string HttpServerUrl { get; set; }
    }
}
