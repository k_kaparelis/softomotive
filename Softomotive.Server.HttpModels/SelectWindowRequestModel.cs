﻿namespace Softomotive.Server.HttpModels
{
    public class SelectWindowRequestModel
    {
        public string ClientConnectionId { get; set; }
        public string WindowName { get; set; }
    }
}
