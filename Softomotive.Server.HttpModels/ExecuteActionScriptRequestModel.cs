﻿namespace Softomotive.Server.HttpModels
{
    public class ExecuteActionScriptRequestModel
    {
        public string ClientConnectionId { get; set; }
        public string ActionScript { get; set; }
    }
}
