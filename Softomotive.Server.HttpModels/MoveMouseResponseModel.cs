﻿namespace Softomotive.Server.HttpModels
{
    public class MoveMouseResponseModel
    {
        public bool InvalidClient { get; set; }
        public bool NoSelectedWindow { get; set; }
    }
}
