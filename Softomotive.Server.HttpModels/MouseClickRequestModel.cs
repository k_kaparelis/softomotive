﻿namespace Softomotive.Server.HttpModels
{
    public class MouseClickRequestModel
    {
        public string ClientConnectionId { get; set; }
    }
}
