﻿namespace Softomotive.Server.HttpModels
{
    public class MoveMouseRequestModel
    {
        public string ClientConnectionId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
