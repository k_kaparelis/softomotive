﻿namespace Softomotive.Server.HttpModels
{
    public class ExecuteActionScriptResponseModel
    {
        public bool InvalidClient { get; set; }
        public string InvalidWindow { get; set; }
        public bool NoSelectedWindow { get; set; }
        public bool InvalidActionScript { get; set; }
        public string InvalidActionScriptAction { get; set; }
    }
}
