﻿namespace Softomotive.Server.HttpModels
{
    public class SendTextRequestModel
    {
        public string ClientConnectionId { get; set; }
        public string Text { get; set; }
    }
}
