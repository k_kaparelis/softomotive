﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Softomotive.Client.Abstractions.Loggers;
using Softomotive.Client.Abstractions.Managers;
using Softomotive.Client.Abstractions.Parsers;
using Softomotive.Client.Exceptions;
using Softomotive.Client.Http.Controllers;
using Softomotive.Client.HttpModels;

namespace Softomotive.Client.UnitTests.Http.Controllers
{
    [TestClass]
    public class CommandControllerTest
    {
        [TestMethod]
        public void TestSelectWindow1()
        {
            const string windowName = "Foo";

            var actionManagerMock = new Mock<IActionManager>();
            var actionScriptParserMock = new Mock<IActionScriptParser>();
            var loggerMock = new Mock<ILogger>();

            actionManagerMock
                .Setup(x => x.SelectWindow(windowName))
                .Throws(new InvalidWindowException(windowName));

            var actionManager = actionManagerMock.Object;
            var actionScriptParser = actionScriptParserMock.Object;
            var logger = loggerMock.Object;

            var controller = new CommandController(actionManager, actionScriptParser, logger);
            var model = new SelectWindowRequestModel() { WindowName = windowName };
            var response = controller.SelectWindow(model);

            Assert.IsTrue(response.InvalidWindow == windowName);
        }

        [TestMethod]
        public void TestSelectWindow2()
        {
            const string windowName = "Foo";

            var actionManagerMock = new Mock<IActionManager>();
            var actionScriptParserMock = new Mock<IActionScriptParser>();
            var loggerMock = new Mock<ILogger>();

            var actionManager = actionManagerMock.Object;
            var actionScriptParser = actionScriptParserMock.Object;
            var logger = loggerMock.Object;

            var controller = new CommandController(actionManager, actionScriptParser, logger);
            var model = new SelectWindowRequestModel() { WindowName = windowName };
            var response = controller.SelectWindow(model);

            Assert.IsTrue(response.InvalidWindow == null);
        }

        [TestMethod]
        public void TestMoveMouse1()
        {
            const int X = 0;
            const int Y = 0;

            var actionManagerMock = new Mock<IActionManager>();
            var actionScriptParserMock = new Mock<IActionScriptParser>();
            var loggerMock = new Mock<ILogger>();

            actionManagerMock
                .Setup(x => x.MouseMove(X, Y))
                .Throws(new NoSelectedWindowException());

            var actionManager = actionManagerMock.Object;
            var actionScriptParser = actionScriptParserMock.Object;
            var logger = loggerMock.Object;

            var controller = new CommandController(actionManager, actionScriptParser, logger);
            var model = new MoveMouseRequestModel() { X = X, Y = Y };
            var response = controller.MoveMouse(model);

            Assert.IsTrue(response.NoSelectedWindow);
        }

        [TestMethod]
        public void TestMoveMouse2()
        {
            const int X = 0;
            const int Y = 0;

            var actionManagerMock = new Mock<IActionManager>();
            var actionScriptParserMock = new Mock<IActionScriptParser>();
            var loggerMock = new Mock<ILogger>();

            var actionManager = actionManagerMock.Object;
            var actionScriptParser = actionScriptParserMock.Object;
            var logger = loggerMock.Object;

            var controller = new CommandController(actionManager, actionScriptParser, logger);
            var model = new MoveMouseRequestModel() { X = X, Y = Y };
            var response = controller.MoveMouse(model);

            Assert.IsTrue(response.NoSelectedWindow == false);
        }
    }
}
