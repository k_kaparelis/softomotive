﻿using System;
using System.Threading.Tasks;

namespace Softomotive.Client.Abstractions.Services
{
    public interface IBootstrapService : IDisposable
    {
        Task<bool> BootstrapAsync();
    }
}
