﻿using System.Threading.Tasks;

namespace Softomotive.Client.Abstractions.Services
{
    public interface IWindowService
    {
        Task StartAsync();
        void Stop();
    }
}
