﻿using System;

namespace Softomotive.Client.Abstractions.Services
{
    public interface IDataService
    {
        void SetActiveWindowCode(IntPtr activeWindowCode);
        IntPtr GetActiveWindowCode();
    }
}
