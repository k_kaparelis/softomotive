﻿using System;

namespace Softomotive.Client.Abstractions.Managers
{
    public interface IWindowManager
    {
        IntPtr GetWindowCode(string windowName);
        void Select(IntPtr windowCode);
    }
}
