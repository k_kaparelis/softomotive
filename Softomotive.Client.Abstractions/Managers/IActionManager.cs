﻿namespace Softomotive.Client.Abstractions.Managers
{
    public interface IActionManager
    {
        void SelectWindow(string windowName);
        void MouseMove(int x, int y);
        void MouseClick();
        void SendText(string text);
    }
}
