﻿namespace Softomotive.Client.Abstractions.Managers
{
    public interface IKeyboardManager
    {
        void Keypress(string text);
    }
}
