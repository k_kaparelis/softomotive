﻿using System;

namespace Softomotive.Client.Abstractions.Managers
{
    public interface IMouseManager
    {
        void Move(IntPtr windowCode, int x, int y);
        void Click();
    }
}
