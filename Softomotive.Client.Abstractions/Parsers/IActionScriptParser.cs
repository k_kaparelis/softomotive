﻿namespace Softomotive.Client.Abstractions.Parsers
{
    public interface IActionScriptParser
    {
        void ParseAndExecute(string actionScript);
    }
}
