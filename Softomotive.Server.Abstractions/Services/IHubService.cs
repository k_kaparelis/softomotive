﻿using System.Threading.Tasks;

namespace Softomotive.Server.Abstractions.Services
{
    public interface IHubService
    {
        Task InvokeClientAddition(string adminConnectionId, string clientConnectionId);
        Task InvokeClientRemoval(string adminConnectionId, string clientConnectionId);
    }
}
