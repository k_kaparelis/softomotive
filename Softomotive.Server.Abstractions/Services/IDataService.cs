﻿using System.Collections.Generic;

namespace Softomotive.Server.Abstractions.Services
{
    public interface IDataService
    {
        void SetAdminConnectionId(string connectionId);
        string GetAdminConnectionId();
        void AddClient(string connectionId, string httpServerUrl);
        bool RemoveClient(string connectionId);
        string GetClientHttpServerUrl(string connectionId);
        IEnumerable<string> GetClientsConnectionIds();
    }
}
