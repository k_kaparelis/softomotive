﻿using System;

namespace Softomotive.Server.Abstractions.Services
{
    public interface IBootstrapService : IDisposable
    {
        bool Bootstrap();
    }
}
