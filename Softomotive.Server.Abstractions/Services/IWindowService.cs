﻿namespace Softomotive.Server.Abstractions.Services
{
    public interface IWindowService
    {
        void Start();
        void Stop();
    }
}
