﻿using Softomotive.Server.Abstractions.Loggers;
using Softomotive.Server.Abstractions.Services;
using Softomotive.Server.HttpModels;
using System.Web.Http;

namespace Softomotive.Server.Http.Controllers
{
    public class ClientController : ApiController
    {
        private IDataService dataService;
        private IHubService hubService;
        private ILogger logger;

        public ClientController(IDataService dataService, IHubService hubService, ILogger logger)
        {
            this.dataService = dataService;
            this.hubService = hubService;
            this.logger = logger;
        }

        [HttpPost]
        public void Connect(ClientConnectionRequestModel model)
        {
            dataService.AddClient(model.ConnectionId, model.HttpServerUrl);
            hubService.InvokeClientAddition(dataService.GetAdminConnectionId(), model.ConnectionId);

            logger.LogInfo($"New client connected: {model.ConnectionId} with url: {model.HttpServerUrl}.");
        }
    }
}
