﻿using Newtonsoft.Json;
using Softomotive.Server.Abstractions.Loggers;
using Softomotive.Server.Abstractions.Services;
using Softomotive.Server.HttpModels;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Softomotive.Server.Http.Controllers
{
    public class AdminController : ApiController
    {
        private IDataService dataService;
        private IHubService hubService;
        private ILogger logger;

        public AdminController(IDataService dataService, IHubService hubService, ILogger logger)
        {
            this.dataService = dataService;
            this.hubService = hubService;
            this.logger = logger;
        }

        [HttpPost]
        public void Register(AdminRegistrationRequestModel model)
        {
            dataService.SetAdminConnectionId(model.ConnectionId);

            logger.LogInfo($"Registered administrator with signalR connection id: {model.ConnectionId}.");
        }

        [HttpGet]
        public IEnumerable<string> GetClientsConnectionIds()
        {
            return dataService.GetClientsConnectionIds();
        }

        [HttpPost]
        public async Task<SelectWindowResponseModel> SelectWindow(SelectWindowRequestModel model)
        {
            var result = new SelectWindowResponseModel();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientHttpServerUrl = dataService.GetClientHttpServerUrl(model.ClientConnectionId);

                    var httpRequestModel = new Client.HttpModels.SelectWindowRequestModel();
                    httpRequestModel.WindowName = model.WindowName;
                    var json = JsonConvert.SerializeObject(httpRequestModel);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync($"{clientHttpServerUrl}/api/command/selectwindow", data);
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var responseModel = JsonConvert.DeserializeObject<Client.HttpModels.SelectWindowResponseModel>(responseBody);

                    result.InvalidWindow = responseModel.InvalidWindow;
                }
            }
            catch (HttpRequestException)
            {
                result.InvalidClient = true;

                await HandleHttpRequestException(model.ClientConnectionId);
            }

            if (result.InvalidClient)
                logger.LogWarn($"Client \"{model.ClientConnectionId}\" didn't respond.");
            else if (result.InvalidWindow != null)
                logger.LogWarn($"There is no window \"{model.WindowName}\" to client with connection id \"{model.ClientConnectionId}\".");
            else
                logger.LogInfo($"Selected successfully window \"${model.WindowName}\" to client with connection id \"{model.ClientConnectionId}\".");

            return result;
        }

        [HttpPost]
        public async Task<MoveMouseResponseModel> MoveMouse(MoveMouseRequestModel model)
        {
            var result = new MoveMouseResponseModel();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientHttpServerUrl = dataService.GetClientHttpServerUrl(model.ClientConnectionId);

                    var httpRequestModel = new Client.HttpModels.MoveMouseRequestModel();
                    httpRequestModel.X = model.X;
                    httpRequestModel.Y = model.Y;
                    var json = JsonConvert.SerializeObject(httpRequestModel);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync($"{clientHttpServerUrl}/api/command/movemouse", data);
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var responseModel = JsonConvert.DeserializeObject<Client.HttpModels.MoveMouseResponseModel>(responseBody);

                    result.NoSelectedWindow = responseModel.NoSelectedWindow;
                }
            }
            catch (HttpRequestException)
            {
                result.InvalidClient = true;

                await HandleHttpRequestException(model.ClientConnectionId);
            }

            if (result.InvalidClient)
                logger.LogWarn($"Client \"{model.ClientConnectionId}\" didn't respond.");
            else if (result.NoSelectedWindow)
                logger.LogWarn($"There is no selected window to client with connection id \"{model.ClientConnectionId}\".");
            else
                logger.LogInfo($"Successfully moved mouse to X: {model.X}, Y: {model.Y} to client with connection id \"{model.ClientConnectionId}\".");

            return result;
        }

        [HttpPost]
        public async Task<MouseClickResponseModel> MouseClick(MouseClickRequestModel model)
        {
            var result = new MouseClickResponseModel();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientHttpServerUrl = dataService.GetClientHttpServerUrl(model.ClientConnectionId);

                    await httpClient.GetAsync($"{clientHttpServerUrl}/api/command/mouseclick");
                }
            }
            catch (HttpRequestException)
            {
                result.InvalidClient = true;

                await HandleHttpRequestException(model.ClientConnectionId);
            }

            if (result.InvalidClient)
                logger.LogWarn($"Client \"{model.ClientConnectionId}\" didn't respond.");
            else
                logger.LogInfo($"Successfully clicked mouse to client with connection id \"{model.ClientConnectionId}\".");

            return result;
        }

        [HttpPost]
        public async Task<SendTextResponseModel> SendText(SendTextRequestModel model)
        {
            var result = new SendTextResponseModel();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientHttpServerUrl = dataService.GetClientHttpServerUrl(model.ClientConnectionId);

                    var httpRequestModel = new Client.HttpModels.SendTextRequestModel();
                    httpRequestModel.Text = model.Text;
                    var json = JsonConvert.SerializeObject(httpRequestModel);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    await httpClient.PostAsync($"{clientHttpServerUrl}/api/command/sendtext", data);
                }
            }
            catch (HttpRequestException)
            {
                result.InvalidClient = true;

                await HandleHttpRequestException(model.ClientConnectionId);
            }

            if (result.InvalidClient)
                logger.LogWarn($"Client \"{model.ClientConnectionId}\" didn't respond.");
            else
                logger.LogInfo($"Successfully sent text to client with connection id \"{model.ClientConnectionId}\".");

            return result;
        }

        [HttpPost]
        public async Task<ExecuteActionScriptResponseModel> ExecuteActionScript(ExecuteActionScriptRequestModel model)
        {
            var result = new ExecuteActionScriptResponseModel();

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientHttpServerUrl = dataService.GetClientHttpServerUrl(model.ClientConnectionId);

                    var httpRequestModel = new Client.HttpModels.ActionScriptRequestModel();
                    httpRequestModel.ActionScript = model.ActionScript;
                    var json = JsonConvert.SerializeObject(httpRequestModel);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync($"{clientHttpServerUrl}/api/command/executeActionScript", data);
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var responseModel = JsonConvert.DeserializeObject<Client.HttpModels.ActionScriptResponseModel>(responseBody);

                    result.InvalidWindow = responseModel.InvalidWindow;
                    result.NoSelectedWindow = responseModel.NoSelectedWindow;
                    result.InvalidActionScript = responseModel.InvalidActionScript;
                    result.InvalidActionScriptAction = responseModel.InvalidActionScriptAction;
                }
            }
            catch (HttpRequestException)
            {
                result.InvalidClient = true;

                await HandleHttpRequestException(model.ClientConnectionId);
            }

            if (result.InvalidClient || result.NoSelectedWindow || result.InvalidActionScript || result.InvalidActionScriptAction != null || result.InvalidClient)
                logger.LogWarn($"Could not complete actionscript to client with connection id \"{model.ClientConnectionId}\".");
            else
                logger.LogInfo($"Successfully sent text to client with connection id \"{model.ClientConnectionId}\".");

            return result;
        }

        private async Task HandleHttpRequestException(string clientConnectionId)
        {
            dataService.RemoveClient(clientConnectionId);
            await hubService.InvokeClientRemoval(dataService.GetAdminConnectionId(), clientConnectionId);
        }
    }
}
