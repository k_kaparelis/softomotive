﻿using Microsoft.AspNet.SignalR;
using Softomotive.Server.Abstractions.Services;
using System.Threading.Tasks;

namespace Softomotive.Server
{
    public interface IHubClient
    {
        Task clientAdded(string connectionId);
        Task clientRemoved(string connectionId);
    }


    public class MyHub : Hub<IHubClient>
    {
        private IDataService dataService;
        private IHubService hubService;

        public MyHub(IDataService dataService, IHubService hubService)
        {
            this.dataService = dataService;
            this.hubService = hubService;
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            // Beware that this doesn't gets called when we terminate clients...

            if (dataService.RemoveClient(Context.ConnectionId))
                hubService.InvokeClientRemoval(dataService.GetAdminConnectionId(), Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }
    }
}
