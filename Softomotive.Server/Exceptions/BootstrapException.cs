﻿using System;

namespace Softomotive.Server.Exceptions
{
    [Serializable]
    public class BootstrapException : Exception
    {
        public BootstrapException() : base() { }
    }
}
