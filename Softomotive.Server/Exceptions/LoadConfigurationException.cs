﻿using System;

namespace Softomotive.Server.Exceptions
{
    [Serializable]
    public class LoadConfigurationException : Exception
    {
        public LoadConfigurationException() : base() { }
    }
}
