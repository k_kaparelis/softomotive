﻿using Microsoft.AspNet.SignalR;
using Softomotive.Server.Abstractions.Services;
using System;
using Topshelf;
using Unity;

namespace Softomotive.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = UnityConfig.Container;

            var windowService = container.Resolve<IWindowService>();
            var rc = HostFactory.Run(x =>
            {
                x.Service<IWindowService>(s =>
                {
                    s.ConstructUsing(name => windowService);
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Softomotive.Server");
                x.SetDisplayName("Softomotive.Server");
                x.SetServiceName("Softomotive.Server");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
