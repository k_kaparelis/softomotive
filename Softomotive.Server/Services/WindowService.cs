﻿using Softomotive.Server.Abstractions.Loggers;
using Softomotive.Server.Abstractions.Services;

namespace Softomotive.Server.Services
{
    public class WindowService : IWindowService
    {
        private IBootstrapService bootstrapper;
        private ILogger logger;

        public WindowService(IBootstrapService bootstrapper, ILogger logger)
        {
            this.bootstrapper = bootstrapper;
            this.logger = logger;
        }

        public void Start()
        {
            var bootstrapped = bootstrapper.Bootstrap();

            if (!bootstrapped)
                logger.LogFatal("Service could not bootstrap successfully.");
        }
        public void Stop()
        {
            bootstrapper.Dispose();
        }
    }
}
