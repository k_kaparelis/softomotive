﻿using Microsoft.AspNet.SignalR;
using Softomotive.Server.Abstractions.Services;
using System.Threading.Tasks;

namespace Softomotive.Server.Services
{
    public class HubService : IHubService
    {
        private IHubContext<IHubClient> hubContext;

        public HubService(IHubContext<IHubClient> hubContext)
        {
            this.hubContext = hubContext;
        }

        public async Task InvokeClientAddition(string adminConnectionId, string clientConnectionId)
        {
            await hubContext.Clients.Client(adminConnectionId).clientAdded(clientConnectionId);
        }

        public async Task InvokeClientRemoval(string adminConnectionId, string clientConnectionId)
        {
            await hubContext.Clients.Client(adminConnectionId).clientRemoved(clientConnectionId);
        }
    }
}
