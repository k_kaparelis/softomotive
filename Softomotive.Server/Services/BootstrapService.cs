﻿using Microsoft.Owin.Hosting;
using Softomotive.Server.Abstractions.Loggers;
using Softomotive.Server.Abstractions.Services;
using Softomotive.Server.Exceptions;
using System;
using System.Configuration;
using System.Reflection;

namespace Softomotive.Server.Services
{
    public class BootstrapService : IBootstrapService
    {
        private ILogger logger;
        private IDisposable httpServer;

        public BootstrapService(ILogger logger)
        {
            this.logger = logger;
        }

        public bool Bootstrap()
        {
            var configuration = GetConfiguration();
            var httpServerUrl = configuration.Item1;

            return StartHttpServer(httpServerUrl);
        }

        private bool StartHttpServer(string httpServerUrl)
        {
            try
            {
                httpServer = WebApp.Start(httpServerUrl);

                logger.LogInfo($"Http server started on {httpServerUrl}.");
                return true;
            }
            catch (TargetInvocationException)
            {
                logger.LogFatal($"Cound not start http server on {httpServerUrl}.");
                return false;
            }
        }

        private Tuple<string> GetConfiguration()
        {
            try
            {
                var httpServerUrl = ConfigurationManager.AppSettings["HttpServerUrl"];

                return Tuple.Create(httpServerUrl);
            }
            catch (Exception)
            {
                logger.LogFatal("Could not load configuration.");

                throw new LoadConfigurationException();
            }
        }

        public void Dispose()
        {
            if (httpServer != null)
                httpServer.Dispose();
        }
    }
}
