﻿using Softomotive.Server.Abstractions.Services;
using System.Collections.Generic;
using System.Linq;

namespace Softomotive.Server.Services
{
    public class DataService : IDataService
    {
        private string adminConnectionId;
        private Dictionary<string, string> clients;

        public DataService()
        {
            clients = new Dictionary<string, string>();
        }

        public void SetAdminConnectionId(string connectionId)
        {
            adminConnectionId = connectionId;
        }

        public string GetAdminConnectionId()
        {
            return adminConnectionId;
        }

        public void AddClient(string connectionId, string httpServerUrl)
        {
            clients.Add(connectionId, httpServerUrl);
        }

        public bool RemoveClient(string connectionId)
        {
            return clients.Remove(connectionId);
        }

        public string GetClientHttpServerUrl(string connectionId)
        {
            return clients[connectionId];
        }

        public IEnumerable<string> GetClientsConnectionIds()
        {
            return clients.Select(x => x.Key);
        }
    }
}
