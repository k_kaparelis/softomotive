﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Cors;
using Owin;
using System.Web.Http;
using Unity.AspNet.WebApi;

namespace Softomotive.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            #region SignalR

            var unityHubActivator = new UnityHubActivator(UnityConfig.Container);
            GlobalHost.DependencyResolver.Register(typeof(IHubActivator), () => unityHubActivator);

            app.MapSignalR();

            #endregion

            #region Configure Web API for self-host. 

            var httpConfig = new HttpConfiguration();

            httpConfig.DependencyResolver = new UnityDependencyResolver(UnityConfig.Container);

            httpConfig.Routes.MapHttpRoute(
                name: "DefaultApi",
                    routeTemplate: "api/{controller}/{action}/{id}",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Index",
                        id = RouteParameter.Optional
                    }
                );

            app.UseWebApi(httpConfig);

            #endregion
        }
    }
}
