﻿namespace Softomotive.Client.HttpModels
{
    public class SelectWindowRequestModel
    {
        public string WindowName { get; set; }
    }
}
