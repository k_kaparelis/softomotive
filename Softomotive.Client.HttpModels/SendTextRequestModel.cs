﻿namespace Softomotive.Client.HttpModels
{
    public class SendTextRequestModel
    {
        public string Text { get; set; }
    }
}
