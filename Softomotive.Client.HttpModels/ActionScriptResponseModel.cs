﻿namespace Softomotive.Client.HttpModels
{
    public class ActionScriptResponseModel
    {
        public string InvalidWindow { get; set; }
        public bool NoSelectedWindow { get; set; }
        public bool InvalidActionScript { get; set; }
        public string InvalidActionScriptAction { get; set; }
    }
}
