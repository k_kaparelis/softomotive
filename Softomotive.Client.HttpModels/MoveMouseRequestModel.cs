﻿namespace Softomotive.Client.HttpModels
{
    public class MoveMouseRequestModel
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
