﻿using Softomotive.Client.Abstractions.Loggers;
using Softomotive.Client.Abstractions.Managers;
using Softomotive.Client.Abstractions.Parsers;
using Softomotive.Client.Exceptions;
using Softomotive.Client.HttpModels;
using System.Web.Http;

namespace Softomotive.Client.Http.Controllers
{
    public class CommandController : ApiController
    {
        private IActionManager actionManager;
        private IActionScriptParser actionScriptParser;
        private ILogger logger;

        public CommandController(IActionManager actionManager, IActionScriptParser actionScriptParser, ILogger logger)
        {
            this.actionManager = actionManager;
            this.actionScriptParser = actionScriptParser;
            this.logger = logger;
        }

        [HttpPost]
        public SelectWindowResponseModel SelectWindow(SelectWindowRequestModel model)
        {
            var result = new SelectWindowResponseModel();

            try
            {
                actionManager.SelectWindow(model.WindowName);
            }
            catch (InvalidWindowException e)
            {
                result.InvalidWindow = e.WindowName;

                logger.LogError($"Exception occurred. Could not find window \"{e.WindowName}\".");
            }

            if (result.InvalidWindow == null)
                logger.LogInfo($"\"{model.WindowName}\" window selected successfully.");

            return result;
        }

        [HttpPost]
        public MoveMouseResponseModel MoveMouse(MoveMouseRequestModel model)
        {
            var result = new MoveMouseResponseModel();

            try
            {
                actionManager.MouseMove(model.X, model.Y);
            }
            catch (NoSelectedWindowException)
            {
                result.NoSelectedWindow = true;

                logger.LogError("Exception occurred. There is no selected window.");
            }

            if (!result.NoSelectedWindow)
                logger.LogInfo($"Mouse moved successfully to X: {model.X}, Y: {model.Y}.");

            return result;
        }

        [HttpGet]
        public void MouseClick()
        {
            actionManager.MouseClick();

            logger.LogInfo("Mouse clicked successfully.");
        }

        [HttpPost]
        public void SendText(SendTextRequestModel model)
        {
            actionManager.SendText(model.Text);

            logger.LogInfo($"Text \"{model.Text}\" sent successfully.");
        }

        [HttpPost]
        public ActionScriptResponseModel ExecuteActionScript(ActionScriptRequestModel model)
        {
            var result = new ActionScriptResponseModel();

            try
            {
                actionScriptParser.ParseAndExecute(model.ActionScript);
            }
            catch (InvalidWindowException e)
            {
                result.InvalidWindow = e.WindowName;

                logger.LogError($"Exception occurred. Could not find window \"{e.WindowName}\".");
            }
            catch (NoSelectedWindowException)
            {
                result.NoSelectedWindow = true;

                logger.LogError("Exception occurred. There is no selected window.");
            }
            catch (InvalidActionScripException)
            {
                result.InvalidActionScript = true;

                logger.LogError("Exception occurred. Invalid actionscript.");
            }
            catch (InvalidActionScriptActionException e)
            {
                result.InvalidActionScriptAction = e.Action;

                logger.LogError($"Exception occurred. Invalid action \"{e.Action}\".");
            }

            if (result.InvalidWindow == null && !result.NoSelectedWindow && !result.InvalidActionScript && result.InvalidActionScriptAction == null)
                logger.LogInfo($"Actionscript \"{model.ActionScript}\" executed successfully.");

            return result;
        }
    }
}
