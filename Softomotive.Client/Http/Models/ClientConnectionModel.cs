﻿namespace Softomotive.Client.Http.Models
{
    public class ClientConnectionModel
    {
        public string ConnectionId { get; set; }
        public string HttpServerUrl { get; set; }
    }
}
