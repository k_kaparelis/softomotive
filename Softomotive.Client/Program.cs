﻿using Softomotive.Client.Abstractions.Services;
using System;
using Topshelf;
using Unity;

namespace Softomotive.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = UnityConfig.Container;
            var windowService = container.Resolve<IWindowService>();

            var rc = HostFactory.Run(x =>
            {
                x.Service<IWindowService>(s =>
                {
                    s.ConstructUsing(name => windowService);
                    s.WhenStarted(tc => tc.StartAsync().Wait());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Softomotive.Client");
                x.SetDisplayName("Softomotive.Client");
                x.SetServiceName("Softomotive.Client");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;

        }
    }
}
