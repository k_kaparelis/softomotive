﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class InvalidActionScripException : Exception
    {
        public InvalidActionScripException(string name) : base(name) { }
    }
}
