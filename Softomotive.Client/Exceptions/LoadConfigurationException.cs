﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class LoadConfigurationException : Exception
    {
        public LoadConfigurationException() : base() { }
    }
}
