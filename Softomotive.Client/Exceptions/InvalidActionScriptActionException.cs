﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class InvalidActionScriptActionException : Exception
    {
        public string Action { get; set; }

        public InvalidActionScriptActionException(string action) : base($"Could not parse {action}")
        {
            Action = action;
        }
    }
}
