﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class NoSelectedWindowException : Exception
    {
        public NoSelectedWindowException() : base() { }
    }
}
