﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class InvalidWindowException : Exception
    {
        public string WindowName { get; set; }

        public InvalidWindowException(string windowName) : base($"Could not find window \"{windowName}\"")
        {
            WindowName = windowName;
        }
    }
}
