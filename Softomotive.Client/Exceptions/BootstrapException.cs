﻿using System;

namespace Softomotive.Client.Exceptions
{
    [Serializable]
    public class BootstrapException : Exception
    {
        public BootstrapException() : base() { }
    }
}
