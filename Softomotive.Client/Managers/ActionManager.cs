﻿using Softomotive.Client.Abstractions.Managers;
using Softomotive.Client.Abstractions.Services;
using Softomotive.Client.Exceptions;
using System;

namespace Softomotive.Client.Managers
{
    public class ActionManager : IActionManager
    {
        private IWindowManager windowManager;
        private IMouseManager mouseManager;
        private IKeyboardManager keyboardManager;
        private IDataService dataService;

        public ActionManager(IWindowManager windowManager, IMouseManager mouseManager, IKeyboardManager keyboardManager, IDataService dataService)
        {
            this.windowManager = windowManager;
            this.mouseManager = mouseManager;
            this.keyboardManager = keyboardManager;
            this.dataService = dataService;
        }

        public void SelectWindow(string windowName)
        {
            var activeWindowCode = windowManager.GetWindowCode(windowName);
            dataService.SetActiveWindowCode(activeWindowCode);

            if (activeWindowCode != IntPtr.Zero)
                windowManager.Select(activeWindowCode);
            else
                throw new InvalidWindowException(windowName);
        }

        public void MouseMove(int x, int y)
        {
            var activeWindowCode = dataService.GetActiveWindowCode();

            if (activeWindowCode == IntPtr.Zero)
                throw new NoSelectedWindowException();

            mouseManager.Move(activeWindowCode, x, y);
        }

        public void MouseClick()
        {
            mouseManager.Click();
        }

        public void SendText(string text)
        {
            keyboardManager.Keypress(text);
        }
    }
}
