﻿using Softomotive.Client.Abstractions.Managers;
using System;
using System.Runtime.InteropServices;

namespace Softomotive.Client.Managers
{
    public class WindowManager : IWindowManager
    {
        [DllImport("User32.dll")]
        private static extern IntPtr FindWindow(string ClassName, string WindowName);

        [DllImport("User32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("User32.dll")]
        private static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();


        private const int SW_RESTORE = 9;


        public IntPtr GetWindowCode(string windowName)
        {
            return FindWindow(null, windowName);
        }

        public void Select(IntPtr windowCode)
        {
            ShowWindow(windowCode, SW_RESTORE);
            SetForegroundWindow(windowCode);
        }
    }
}
