﻿using Softomotive.Client.Abstractions.Managers;
using System.Windows.Forms;

namespace Softomotive.Client.Managers
{
    public class KeyboardManager : IKeyboardManager
    {
        public void Keypress(string text)
        {
            SendKeys.SendWait(text);
        }
    }
}
