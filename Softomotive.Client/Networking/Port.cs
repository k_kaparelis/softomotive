﻿using System.Net;
using System.Net.Sockets;

namespace Softomotive.Client.Networking
{
    public static class Port
    {
        public static int GetNextAvailable()
        {
            var l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }
    }
}
