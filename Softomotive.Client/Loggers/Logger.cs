﻿using log4net;
using Softomotive.Client.Abstractions.Loggers;
using System.Reflection;

namespace Softomotive.Client.Loggers
{
    public class Logger : ILogger
    {
        private static readonly ILog log4netLogger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void LogDebug(string message)
        {
            log4netLogger.Debug(message);
        }

        public void LogInfo(string message)
        {
            log4netLogger.Info(message);
        }

        public void LogWarn(string message)
        {
            log4netLogger.Warn(message);
        }

        public void LogError(string message)
        {
            log4netLogger.Error(message);
        }

        public void LogFatal(string message)
        {
            log4netLogger.Fatal(message);
        }
    }
}
