﻿using Microsoft.Owin.Cors;
using Owin;
using System.Web.Http;
using Unity.AspNet.WebApi;

namespace Softomotive.Client
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            #region Configure Web API for self-host. 

            var httpConfig = new HttpConfiguration();

            httpConfig.DependencyResolver = new UnityDependencyResolver(UnityConfig.Container);

            httpConfig.Routes.MapHttpRoute(
                name: "DefaultApi",
                    routeTemplate: "api/{controller}/{action}/{id}",
                    defaults: new
                    {
                        controller = "Home",
                        action = "Index",
                        id = RouteParameter.Optional
                    }
                );

            app.UseWebApi(httpConfig);

            #endregion
        }
    }
}
