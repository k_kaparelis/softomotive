﻿using Softomotive.Client.Abstractions.Managers;
using Softomotive.Client.Abstractions.Parsers;
using Softomotive.Client.Exceptions;
using System;
using System.Linq;

namespace Softomotive.Client.Parsers
{
    public class ActionScriptParser : IActionScriptParser
    {
        private IActionManager actionManager;

        private const string SELECT_WINDOW = "select-window";
        private const string MOUSE_MOVE = "mouse-move";
        private const string MOUSE_CLICK = "mouse-click";
        private const string SEND_KEYS = "send-keys";

        private const string SINGLE_CLICK = "single-click";

        public ActionScriptParser(IActionManager actionManager)
        {
            this.actionManager = actionManager;
        }

        public void ParseAndExecute(string actionScript)
        {
            if (actionScript == null || actionScript.Trim() == null)
                throw new InvalidActionScripException("Actionscript is invalid");

            foreach (var action in actionScript.Split(new[] { '\r', '\n' }))
            {
                var pieces = action.Split(new[] { ' ' }, 2);
                var command = pieces.ElementAtOrDefault(0);
                var parameters = pieces.ElementAtOrDefault(1);

                if (string.IsNullOrWhiteSpace(command) || string.IsNullOrWhiteSpace(parameters))
                    throw new InvalidActionScriptActionException(action);

                command = command.ToLower();

                switch (command)
                {
                    case SELECT_WINDOW:
                        SelectWindow(parameters);
                        break;

                    case MOUSE_MOVE:
                        MouseMove(action, parameters);
                        break;

                    case MOUSE_CLICK:
                        MouseClick(action, parameters);
                        break;

                    case SEND_KEYS:
                        SendKeys(parameters);
                        break;

                    default:
                        throw new InvalidActionScriptActionException(action);
                }
            }
        }

        private void SelectWindow(string parameters)
        {
            var windowName = parameters.Replace("\"", string.Empty);

            actionManager.SelectWindow(windowName);
        }

        private void MouseMove(string action, string parameters)
        {
            var xyPieces = parameters.Trim().Split(',');
            var xStr = xyPieces.ElementAtOrDefault(0);
            var yStr = xyPieces.ElementAtOrDefault(1);

            if (string.IsNullOrWhiteSpace(xStr) || string.IsNullOrWhiteSpace(yStr))
                throw new InvalidActionScriptActionException(action);

            var x = 0;
            var y = 0;
            if (!int.TryParse(xStr, out x) || !int.TryParse(yStr, out y))
                throw new InvalidActionScriptActionException(action);

            actionManager.MouseMove(x, y);
        }

        private void MouseClick(string action, string parameters)
        {
            if (parameters.Equals(SINGLE_CLICK, StringComparison.InvariantCultureIgnoreCase))
                actionManager.MouseClick();
            else
                throw new InvalidActionScriptActionException(action);
        }

        private void SendKeys(string parameters)
        {
            var text = parameters.Replace("\"", string.Empty);

            actionManager.SendText(text);
        }
    }
}
