﻿using Microsoft.AspNet.SignalR.Client;
using Microsoft.Owin.Hosting;
using Newtonsoft.Json;
using Softomotive.Client.Abstractions.Loggers;
using Softomotive.Client.Abstractions.Services;
using Softomotive.Client.Exceptions;
using Softomotive.Client.Http.Models;
using Softomotive.Client.Networking;
using System;
using System.Configuration;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Softomotive.Client.Services
{
    public class BootstrapService : IBootstrapService
    {
        private ILogger logger;
        private IDisposable httpServer;
        private IDisposable hubConnection;

        public BootstrapService(ILogger logger)
        {
            this.logger = logger;
        }

        public async Task<bool> BootstrapAsync()
        {
            var configuration = GetConfiguration();
            var remoteHttpServerUrl = configuration.Item1;
            var hubName = configuration.Item2;
            var localHttpServerPort = configuration.Item3;

            var httpServerDetails = StartHttpServer(localHttpServerPort);
            var httpServerStarted = httpServerDetails.Item1;
            var httpServerUrl = httpServerDetails.Item2;

            if (!httpServerStarted)
                return false;

            var signalRConnectionDetails = await ConnectToSignalRServerAsync(remoteHttpServerUrl, hubName);
            var connectedToSignalRServer = signalRConnectionDetails.Item1;
            var signalRConnectionId = signalRConnectionDetails.Item2;

            if (!connectedToSignalRServer)
                return false;

            var registeredToServer = await RegisterToServerAsync(httpServerUrl, remoteHttpServerUrl, signalRConnectionId);

            if (!registeredToServer)
                return false;


            return true;
        }

        private Tuple<bool, string> StartHttpServer(int? localHttpServerPort)
        {
            var localHttpServerUrl = $"http://localhost:{ localHttpServerPort ?? Port.GetNextAvailable()}";

            try
            {
                httpServer = WebApp.Start(localHttpServerUrl);

                logger.LogInfo($"Http server started on {localHttpServerUrl}.");
                return Tuple.Create(true, localHttpServerUrl);
            }
            catch (TargetInvocationException)
            {
                logger.LogFatal($"Cound not start http server on {localHttpServerUrl}.");
                return Tuple.Create<bool, string>(true, null);
            }
        }

        private async Task<Tuple<bool, string>> ConnectToSignalRServerAsync(string serverUrl, string hubName)
        {
            var hubConnection = new HubConnection(serverUrl);
            this.hubConnection = hubConnection;
            hubConnection.CreateHubProxy(hubName);

            try
            {
                await hubConnection.Start();
            }
            catch (HttpRequestException)
            {
                logger.LogFatal($"Could not connect to signalR server \"{serverUrl}\", with hub name \"{hubName}\".");
                return Tuple.Create<bool, string>(false, null);
            }

            logger.LogInfo("Connected to signalR server.");

            return Tuple.Create(true, hubConnection.ConnectionId);
        }

        private async Task<bool> RegisterToServerAsync(string localHttpServerUrl, string remoteHttpServerUrl, string signalRConnectionId)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var clientConnectionModel = new ClientConnectionModel() { ConnectionId = signalRConnectionId, HttpServerUrl = localHttpServerUrl };
                    var json = JsonConvert.SerializeObject(clientConnectionModel);
                    var data = new StringContent(json, Encoding.UTF8, "application/json");
                    await httpClient.PostAsync($"{remoteHttpServerUrl}/api/client/connect", data);

                    logger.LogInfo($"Registered to server with connection id: {signalRConnectionId}.");

                    return true;
                }
            }
            catch (HttpRequestException)
            {
                logger.LogFatal("Could not register to server.");

                return false;
            }
        }

        private Tuple<string, string, int?> GetConfiguration()
        {
            try
            {
                var remoteHttpServerUrl = ConfigurationManager.AppSettings["RemoteHttpServerUrl"];
                var hubName = ConfigurationManager.AppSettings["HubName"];

                int? localHttpServerPort = null;
                if (int.TryParse(ConfigurationManager.AppSettings["LocalHttpServerPort"], out var port))
                    localHttpServerPort = port;

                return Tuple.Create(remoteHttpServerUrl, hubName, localHttpServerPort);
            }
            catch (Exception)
            {
                logger.LogFatal("Could not load configuration.");

                throw new LoadConfigurationException();
            }
        }

        public void Dispose()
        {
            if (httpServer != null)
                httpServer.Dispose();

            if (hubConnection != null)
                hubConnection.Dispose();
        }
    }
}
