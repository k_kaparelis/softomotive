﻿using Softomotive.Client.Abstractions.Loggers;
using Softomotive.Client.Abstractions.Services;
using System.Threading.Tasks;

namespace Softomotive.Client.Services
{
    public class WindowService : IWindowService
    {
        private IBootstrapService bootstrapper;
        private ILogger logger;

        public WindowService(IBootstrapService bootstrapper, ILogger logger)
        {
            this.bootstrapper = bootstrapper;
            this.logger = logger;
        }

        public async Task StartAsync()
        {
            var bootstrapped = await bootstrapper.BootstrapAsync();

            if (!bootstrapped)
                logger.LogFatal("Service could not bootstrap successfully.");
        }
        public void Stop()
        {
            bootstrapper.Dispose();
        }
    }
}
