﻿using Softomotive.Client.Abstractions.Services;
using System;

namespace Softomotive.Client.Services
{
    public class DataService : IDataService
    {
        private IntPtr activeWindowCode;

        public void SetActiveWindowCode(IntPtr activeWindowCode)
        {
            this.activeWindowCode = activeWindowCode;
        }

        public IntPtr GetActiveWindowCode()
        {
            return activeWindowCode;
        }
    }
}
